#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <conio.h>
#include <windows.h>

#define CONSOLE_DEFAULT_COLOR FOREGROUND_BLUE|FOREGROUND_GREEN|FOREGROUND_RED|FOREGROUND_INTENSITY
#define CONSOLE_COLOR_GRAY FOREGROUND_BLUE|FOREGROUND_GREEN|FOREGROUND_RED
#define CONSOLE_COLOR_GREEN FOREGROUND_GREEN|FOREGROUND_INTENSITY

#define CONSOLE_COLOR_BG_GREEN BACKGROUND_GREEN;

#define COLOR_TYPE_HEADER 1
#define COLOR_TYPE_LINE_LETTERS 2
#define COLOR_TYPE_LINE_DIGITS 3

#define CONSOLE_LINE_COUNT 70

using namespace std;



void print_console();
void print_batlefield_proto();
void print_batlefield_line(const char* line, int* colors, bool newLine);
void set_console_colors(int colorType, int* arColors);

void print_console()
{
    print_batlefield_proto();
}

void set_console_colors(int colorType, int* arColors)
{
    memset(arColors, -1, sizeof(int)*CONSOLE_LINE_COUNT);
    switch(colorType){
    case COLOR_TYPE_HEADER:
        arColors[0] = CONSOLE_COLOR_BG_GREEN;
        arColors[22] = CONSOLE_DEFAULT_COLOR;
        arColors[37] = CONSOLE_COLOR_BG_GREEN;
        break;
    case COLOR_TYPE_LINE_LETTERS:
        arColors[0] = BACKGROUND_GREEN|FOREGROUND_BLUE|FOREGROUND_GREEN|FOREGROUND_RED|FOREGROUND_INTENSITY;
        arColors[22] = CONSOLE_DEFAULT_COLOR;
        arColors[37] = BACKGROUND_GREEN|FOREGROUND_BLUE|FOREGROUND_GREEN|FOREGROUND_RED|FOREGROUND_INTENSITY;
        break;
    case COLOR_TYPE_LINE_DIGITS:
        arColors[0] = BACKGROUND_GREEN|FOREGROUND_BLUE|FOREGROUND_GREEN|FOREGROUND_RED|FOREGROUND_INTENSITY;
        arColors[2] = CONSOLE_DEFAULT_COLOR;
        arColors[37] = BACKGROUND_GREEN|FOREGROUND_BLUE|FOREGROUND_GREEN|FOREGROUND_RED|FOREGROUND_INTENSITY;
        arColors[39] = CONSOLE_DEFAULT_COLOR;
        break;
    }
}



void print_batlefield_proto()
{

    printf("\n");
    int* arColors = new int[CONSOLE_LINE_COUNT];
    //memset(arColors, -1, sizeof(int)*CONSOLE_LINE_COUNT);
    set_console_colors(COLOR_TYPE_HEADER, arColors);
    print_batlefield_line("   ��� �������:                         ���������:         ", arColors, true);
    set_console_colors(COLOR_TYPE_LINE_LETTERS, arColors);
    print_batlefield_line("   � � � � � � � � � �                  � � � � � � � � � �", arColors, true);
    set_console_colors(COLOR_TYPE_LINE_DIGITS, arColors);
    print_batlefield_line(" 1 W W W X 0 - - 0 X X                1 0 x 0 - - - - - - - ", arColors, true);
    print_batlefield_line(" 2 0 - - - - - - 0 0 0                2 - x - - - - - - - - ", arColors, true);
    print_batlefield_line(" 3 - - - - - 0 0 0 - -                3 - - - - - - - - 0 0 ", arColors, true);
    print_batlefield_line(" 4 - - - - - 0 X 0 - -                4 - - - - - - - - 0 X ", arColors, true);
    print_batlefield_line(" 5 - W - - - 0 0 0 - -                5 - - - - - - - - 0 X ", arColors, true);
    print_batlefield_line(" 6 - - - - - - - - - -                6 0 - - - - - - - 0 X ", arColors, true);
    print_batlefield_line(" 7 - - - - - - - - - -                7 - - - - - - - - 0 0 ", arColors, true);
    print_batlefield_line(" 8 0 W W 0 - - W W - W                8 - - - - - - - - - - ", arColors, true);
    print_batlefield_line(" 9 - 0 - - - - - - - -                9 - - x - - - - - - - ", arColors, true);
    print_batlefield_line("10 0 W - W W W - W W W               10 - - - - - - - - - - ", arColors, true);
    printf("\n\n");
	/*
	printf("   ��� �������:                         ���������:          \n");
	printf("   � � � � � � � � � �                  � � � � � � � � � � \n");
	printf(" 1 W W W X 0 - - 0 X X                1 0 x 0 - - - - - - - \n");
	printf(" 2 0 - - - - - - 0 0 0                2 - x - - - - - - - - \n");
	printf(" 3 - - - - - 0 0 0 - -                3 - - - - - - - - 0 0 \n");
	printf(" 4 - - - - - 0 X 0 - -                4 - - - - - - - - 0 X \n");
	printf(" 5 - W - - - 0 0 0 - -                5 - - - - - - - - 0 X \n");
	printf(" 6 - - - - - - - - - -                6 0 - - - - - - - 0 X \n");
	printf(" 7 - - - - - - - - - -                7 - - - - - - - - 0 0 \n");
	printf(" 8 0 W W 0 - - W W - W                8 - - - - - - - - - - \n");
	printf(" 9 - 0 - - - - - - - -                9 - - x - - - - - - - \n");
	printf("10 0 W - W W W - W W W               10 - - - - - - - - - - \n\n");
    */
}

void print_batlefield_line(const char* line, int* colors, bool newLine=false)
{
	HANDLE hstdout = GetStdHandle( STD_OUTPUT_HANDLE );
    // Remember how things were when we started
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	GetConsoleScreenBufferInfo( hstdout, &csbi );
	// Tell the user how to stop
	SetConsoleTextAttribute( hstdout, CONSOLE_DEFAULT_COLOR );
    for(int i=0; i<strlen(line)&&i<CONSOLE_LINE_COUNT; i++){
        if(colors != NULL && colors[i]>-1){
            SetConsoleTextAttribute( hstdout, colors[i] );
        }
        printf("%c", line[i]);
    }
	// Keep users happy
	SetConsoleTextAttribute( hstdout, csbi.wAttributes );
	if(newLine)
        printf("\n");
}


int main()
{


    system("chcp 1251 > nul");
    print_console();
    return 0;
}
